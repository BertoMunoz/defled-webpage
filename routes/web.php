<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth routes
Auth::routes();


// Web page
Route::get('/', function () { return view('welcome'); })->name('welcome');
Route::get('/about', function () { return view('about'); })->name('about');
Route::get('/albums', 'ContactController@index')->name('albums');
Route::get('/tracks', 'ContactController@index')->name('tracks');
Route::get('/gallery', 'ContactController@index')->name('gallery');
Route::get('/lives', 'ContactController@index')->name('lives');

Route::get('/contact', 'ContactController@index')->name('contact');

Route::get('/podcasts', 'HomeController@index')->name('podcasts');


// Admin Panel
Route::get('/home', 'HomeController@index')->name('home');
