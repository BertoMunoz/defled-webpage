<nav class="navbar navbar-expand-md navbar-light bg-white">
    
    <div class="container">

        <a class="navbar-brand" href="{{ route('welcome') }}">
            {{ config('app.name', 'DEFLED') }}
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto"></ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('welcome') ? 'active' : '' }}" href="{{ route('welcome') }}" href="{{ route('welcome') }}">News</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('about') ? 'active' : '' }}" href="{{ route('about') }}">About</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('albums') ? 'active' : '' }}" href="{{ route('albums') }}">Albums</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('tracks') ? 'active' : '' }}" href="{{ route('tracks') }}">Tracks</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('lives') ? 'active' : '' }}" href="{{ route('lives') }}">Lives</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('podcasts') ? 'active' : '' }}" href="{{ route('podcasts') }}">Podcasts</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('gallery') ? 'active' : '' }}" href="{{ route('gallery') }}">Gallery</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('contact') ? 'active' : '' }}" href="{{ route('contact') }}">Contact</a>
                </li>

            </ul>
            
        </div>
    </div>
</nav>