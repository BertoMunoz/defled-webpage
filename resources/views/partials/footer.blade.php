<footer class="sticky-footer">

	<div class="footer-section">
		<h4>Heading 1</h4>
		<ul>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>

	<div class="footer-section">
		<h4>Discograficas</h4>
		<ul>
			<li>Modern Obscure Music</li>
			<li>La Cúpula Music</li>
			<li>Sores</li>
		</ul>
	</div>

	<div class="footer-section">
		<h4>Disponible en</h4>
		<ul>
			<li><a href="" target="_blank">SoundCloud</a></li>
			<li><a href="" target="_blank">Youtube</a></li>
			<li><a href="" target="_blank">Spotify</a></li>
			<li><a href="" target="_blank">Google Play music</a></li>
			<li><a href="" target="_blank">Deezer</a></li>
		</ul>
	</div>

	<div class="copy-wrapper">
		<p>&copy; Copyright 2020 - Defled. All rights reserved.</p>
	</div>

</footer>