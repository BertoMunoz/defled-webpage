<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">

<title> {{ config('app.name', 'DEFLED') }} | @yield('title') </title>

<meta name="description" content="">

<link rel="canonical" href="https://www.defled.com/">
<link rel="alternate" hreflang="en" href="https://www.defled.com/en">

<meta name="ROBOTS" content="ALL, INDEX, FOLLOW">
<meta property="og:url" content="https://www.defled.com/">
<meta property="og:type" content="website">
<meta property="og:title" content="">
<meta property="og:description" content="">
<meta property="og:image" content="">
<meta property="og:image:alt" content="DEFLED">

<link rel="apple-touch-icon" href="">
<link rel="apple-touch-icon" sizes="72x72" href="">
<link rel="apple-touch-icon" sizes="114x114" href="">
<link rel="apple-touch-icon" sizes="144x144" href="">

<meta name="keywords" content="music, production, ambient">
<meta name="author" content="Alberto Muñoz Cabrer">
<meta name="csrf-token" content="{{ csrf_token() }}" />

<link rel="icon" href="{{ URL::asset('/favicon.ico') }}" type="image/x-icon"/>

{{-- Styles --}}
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

{{-- Material Icons --}}
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
