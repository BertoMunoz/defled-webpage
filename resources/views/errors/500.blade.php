@extends('layouts.app')

@section('title', 'Unauthorized error')

@section('content')

<div class="error-container">
	<h1 class="error-title">403</h1>  
	<p class="error-text">Forbidden</p>
	<a class="basic-button" href="{{route('home')}}">Return to Home Page</a>
</div>


@endsection