<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  @include('partials.head')
</head>

<body>

    {{-- Navbar --}}
    @section('navbar')
        @include('partials.navbar')
    @show

    {{-- Main Container --}}
    <main id="app" class="container d-flex justify-content-center">

        <div class="row">

            @yield('content')

        </div> 

    </main>

    {{-- Footer --}}
    @section('footer')
        @include('partials.footer')
    @show

</body>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

@yield('javascript')

</html>
