@component('mail::message')
	# Contact Form

	<p>From: {{ $contact['name'] }}</p>

	<p>Email: {{ $contact['email'] }}</p>

	<p>Phone: {{ $contact['phone'] }}</p>

	{{ $contact['message'] }}

	Thanks,<br>
	{{ config('app.name') }}
@endcomponent