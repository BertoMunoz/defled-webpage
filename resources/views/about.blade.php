@extends('layouts.app')

@section('title', 'About')

@section('content')
<div class="flex-center position-ref full-height">

	<h2>DEFLED</h2>

	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam, quibusdam, numquam. Voluptatibus facere tempora voluptatem error rerum. Commodi voluptate, perspiciatis veniam, voluptatem amet labore quos perferendis similique aliquam nisi mollitia!</p>

	<img src="" alt="">

	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut modi ratione voluptatibus, laboriosam deleniti pariatur, id ipsa corporis. Dolorum alias, ex eius molestias nam vero minus ab quod! Nobis, expedita.</p>

</div>
@endsection